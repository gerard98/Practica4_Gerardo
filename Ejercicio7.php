<html>
	<head>
		<title>Ejemplo Resolver Operaciones</title>
	</head>
	<body>
		<h1>Resuelva las expresiones presentadas</h1>
	
		<?php
			$i = 9;
			$f = 33.5;
			$c = 'X';
			
			
			echo ($i >= 6) && ($c == 'X'), "<br>";
			echo ($i >= 6) || ($c == 12), "<br>";
			echo ($f < 11) && ($i > 100), "<br>";
			echo ($c != 'P') || (($i + $f) <= 10), "<br>";
			echo  $i + $f <= 10 ,"<br>";
			echo  $i >= 6 && $c == 'X', "<br>";
			echo  $c != 'P' || $i + $f <= 10,  "<br>";
			
/*Resultados
Expresion ($i >= 6) && ($c == ‘X’)      	TRUE
Expresion($i >= 6) || ($c == 12)			TRUE
Expresion($f < 11) && ($i > 100)			FALSE
Expresion($c != ‘P’) || (($i + $f) <= 10)	TRUE 
Expresion $i + $f <= 10						FALSE
Expresion $i >= 6 && $c == ‘X’				TRUE 
Expresion $c != ‘P’ || $i + $f <= 10		TRUE
*/



			
		?>
	</body>
</html>